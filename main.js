var sigmets = new Array()
fetch('https://api.met.no/weatherapi/sigmets/2.0/').then(res => res.clone().text()).then(res => {
    metmsgs = res.match(/ZCZC([\s\S]*?=)/g)
    if (!metmsgs) {
        document.getElementsByTagName("body")[0].innerHTML = "<div style='width:100%;padding-top:40vh;text-align:center;'>No SIGMETS/AIRMETS published for ENOR FIR at this time.</div>"
        return false;
    }
    metmsgs.filter(sigmet => {
        if (Array.isArray(sigmet.match(/AIRMET|SIGMET/g))) {
            sigmets.push(sigmet.replace(/\r\n|\n|\r|ZCZC/gm, ""))
        }
    })

    // Fjerner duplicates
    sigmets = [...new Set(sigmets)];
    var parsed = new Array();
    var arrCancelled = new Array();

    // Sjekker først om det er noen cancel-meldinger (cnlPattern). Hvis ja så testes ikke meldingen med main pattern (det vil feile)
    var cnlPattern = /(CNL \w{3}MET \w\d{2}) (\d{6})\/(\d{6})=/g;
    var pattern = /\w+\s(\w{4})\s(\d{6})\s?(\w{4})\s(\w+)\s(\w+)\sVALID\s(\d{6})\/(\d{6})\s(\w{4})\s?-\s?(\w{4}\s[\w\s]*FIR)\s([\w\s]*(?=WI))WI\s([NSEW\d\s-]*)\s(SFC|FL\d{3}|\d{4}FT)?\/?((?:TOP )?FL\d{3}|\d{3}|\d{4}FT)\s([\w\s]*)=/g;
    sigmets.forEach(sigmet => {
        cnlPattern.lastIndex = 0;
        var cnltest = cnlPattern.exec(sigmet); // Returnerer null hvis den ikke matcher
        if (cnltest) {
            arrCanceled.push(cnltest[1])
            return;
        }
        
        // Ekstraher avsender, utstedt tid, betegnelse, gyldighetsperiode, FIR, Met.fenomen, koordinater, høyde og trend
        pattern.lastIndex = 0;
        parsed.push(pattern.exec(sigmet))
    })
    
    // Fjerne kansellerte meldinger fra parsed
    // Todo: Matche klokkeidentifikatorer i cnlMsg også, ikke bare meldingsid. Ellers risikerer man at f.eks. D01 for natt til den 17.
    // blir kansellert fordi det er utstedt kansellering på D01 utstedt om kvelden den 16. (Antar at dette skjer uhyre sjelden).
    arrCancelled.forEach(cnlMsg => {
        parsed = parsed.filter(msg => msg[4] + " " + msg[5] != cnlMsg)
    })

    var geojson = {
        "name": "NewFeatureType",
        "type": "FeatureCollection",
        "features": []
    };

    // Capture group 12 har alle koords i seg. Disse må parses og konverteres med funksjonen under.
    var pattern = /(N|S)(\d{2})(\d{2})(\d{0,2})\s?(E|W)(\d{3})(\d{2})(\d{0,2})/g;
    for (x = 0; x < parsed.length; x++) {
        var arrGJCoords = new Array()
        var strCoords = parsed[x][11].replace(/ - |- | -/g, "-")
        var arrCoords = strCoords.split("-")
        var feature = {
            "type": "Feature",
            "id": x,
            "geometry": {
                "type": "Polygon",
                "coordinates": []
            },
            "properties": {
                "fullText": parsed[x][0],
                "comCentre": parsed[x][1],
                "issuedTime": parsed[x][2],
                "locationIndicator": parsed[x][3],
                "messageType": parsed[x][4],
                "sequenceNo": parsed[x][5],
                "validFrom": parsed[x][6],
                "validTo": parsed[x][7],
                "mwo": parsed[x][8],
                "FIR": parsed[x][9],
                "phenomenon": parsed[x][10],
                "altFrom": parsed[x][12],
                "altTo": parsed[x][13],
                "forecast": parsed[x][14]
            }
        };

        for (y = 0; y < arrCoords.length; y++) {
            pattern.lastIndex = 0;
            s = pattern.exec(arrCoords[y]);
            arrGJCoords.push(DMS(s[5], s[6], s[7], s[8], s[1], s[2], s[3], s[4]));
        }

        feature.geometry.coordinates.push(arrGJCoords);
        geojson.features.push(feature);

    }
    document.getElementsByTagName("body")[0].innerHTML = "<div id='mapDiv' style='width:98vw;height:97vh;padding:auto;'></div>"
    var params = new Object();
    params.id = 'mapDiv';
    params.geojson = geojson;
    initMap(params);
})

// Konverterer grad,min,sek til Google-akseptert LatLng
function DMS(ns, lat, latm, lats, ew, lon, lonm, lons) {
    var ret = new Object()
    if (ns == "S") {
        lat = -lat;
    }
    if (ew == "W") {
        lon = -lon;
    }
    lat = Math.sign(lat) * (Math.abs(lat) + (latm / 60) + (lats / 3600));
    lon = Math.sign(lon) * (Math.abs(lon) + (lonm / 60) + (lons / 3600));
    return [lat, lon];
}

var infowindow = new Array()
function initMap(params) {
    var bounds = new google.maps.LatLngBounds();
    var soner = new google.maps.Data();

    var map = new google.maps.Map(document.getElementById(params.id), {
        zoom: 5,
        center: {lat: 66, lng: 15},
        streetViewControl: false
    });

    soner.addGeoJson(params.geojson);

    soner.setMap(map)

    soner.forEach(sone => {
        var x = 0;
        var cLat = 0;
        var cLng = 0;
        sone.getGeometry().forEachLatLng(coor => {
            bounds.extend(coor);
            cLat = cLat + coor.lat();
            cLng = cLng + coor.lng();
            x++;
        })

        var id = sone.getId();
        var content = sone.getProperty("messageType") + " " + sone.getProperty("sequenceNo") + " ";
        content += "VALID " + sone.getProperty("validFrom") + "/" + sone.getProperty("validTo") + "<br>";
        content += sone.getProperty("FIR") + " " + sone.getProperty("phenomenon") + " ";
        content += sone.getProperty("altFrom") + "/" + sone.getProperty("altTo") + " ";
        content += sone.getProperty("forecast") + "<br>";
        content += "<a style='cursor:pointer;text-decoration:underline;color:blue;' onclick=\"document.getElementById('pFull"+id+"').style.display='block';\">Full text ...</a>";
	content += "<p id='pFull"+id+"' style='display:none'>" + sone.getProperty("fullText") + "</p";

        infowindow[id] = new google.maps.InfoWindow({content: content, position: {lat: cLat / x, lng: cLng / x}})
        infowindow[id].open(map)
    })

    map.fitBounds(bounds);

    soner.addListener('click', function (ev) {
        var id = ev.feature.getId()
        infowindow[id].open(map);
    });
}





